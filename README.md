# Mobile App Development Test

In this exercise, it is required to use [Google Firestore](https://cloud.google.com/firestore?utm_source=google&utm_medium=cpc&utm_campaign=emea-it-all-en-dr-bkws-all-all-trial-b-gcp-1009139&utm_content=text-ad-none-any-DEV_c-CRE_485007165071-ADGP_Hybrid%20%7C%20AW%20SEM%20%7C%20BKWS%20~%20BMM_IT_EN_Databases_Firestore_Generic-KWID_43700059352313507-kwd-1115197366247-userloc_1008736&utm_term=KW_%2Bfirestore-NET_g-PLAC_&&gclid=Cj0KCQiAlZH_BRCgARIsAAZHSBmqeHuMqyzzZ_jfYjoIcKwDyiSGP_I0HkOz3BwTHUTWybYfsi89wrkaAkRqEALw_wcB) to store data. You can mock the server-side for simplicity, but the calls to Firestore must be implemented.  

## Requirements 

The application should support two languages (ITA and ENG) according to the system language. The application will show three different screens. 

### Registration Screen

Registration screen contains a form with the following fields: 

* Name
* Last name
* Email address
* Mobile Phone number
* Password

Note: the registration and login phases can be skipped. However, the above information must be pushed to Firestore. 

### Data Entry Screen

On this screen, the user has the opportunity to complete a form with her data. In particular, the following fields will be shown:

* Gender 
* Date of Birth
* Favourite dish 
* Favourite soccer team

### Upload file Screen

On this screen, the user has the opportunity to take a picture of a document that she/he wants to upload. The user can discard a picture, take multiple pictures to scan different pages, and confirm a scan. Once confirmed the scan will be pushed to Firebase. 


## General Requirements

* You will use a cross-platform framework for mobile app development, e.g., flutter or react native. 

* Minimum code coverage for unit testing should be 70%

* The sources of your application must be delivered.  Add a README that explains how to compile and run the code.

* You must release your work with an OSI-approved open source license of your choice.

* Add the code to your own Github (or Bitbucket) account and send us the link.
